The BusVoodoo is a multi-protocol debugging adapter.
This tool allows to quickly communicate with various other electronic devices.

connectors
==========

Connect the adapter to the computer using the USB port (mini-B).
It should appear as a serial port (using the USB CDC ACM class).
Using a serial terminal access the menu and operate the device (the serial baud rate and configuration are irrelevant).

Connect the adapter to the target to debug using the I/O connector (2x5 pins, 2.54 mm pitch IDC connector).

I/O connector pinout:
      +---+
  GND |1 2| 5V
  3V3 |3 4| xV
I/O-1  5 6| I/O-2
I/O-3 |7 8| I/O-4
I/O-5 |9 0| I/O-6
      +---+

- The 5V (up to 400 mA) and 3V3 (up to 250 mA)  power outputs can be enabled in the menu (commonly).
- The xV pin is an adjustable power output from 0 to 4.8V (up to 400 mA). The xV pin can also be used as ADC (0 to 6 V) or power input for the embedded pull-up resistors.
- The 6 I/O pins support numerous electrical communication protocols. The pins can be driven in push-pull mode at 3.3V, or in open drain mode at 1.6 to 5.5V using embedded 2 kOhms resistors and the xV pin. All signals are protected with 220 Ohms resistors.

The BusVoodoo comes in two versions: light and full.
The full version has two additional connectors: one for an I2C OLED display (to display the current protocol pinout), and one for RS and CAN protocols.

RS/CAN pinout:
+-+
|1| 12V
|2| RS-232 RX, CAN H
|3| RS-232 TX, CAN L
|4| RS-232 RTS, RS-485/RS-422 B
|5| RS-232 CTS, RS-485/RS-422 A
+-+

The 12V pin is an adjustable 3.3 to 18 V voltage regulator (up to 40 mA) meant for pins requiring higher voltages to erase or re-program.

The light and full versions use the same board and firmware.
The full version only has additional components (voltage booster, transceivers), all on the back side.

protocols
=========

The BusVoodoo supports the following protocol in hardware (tahnks to the STM32F103RC micro-controller):
- 2 UARTs, one with hardware flow control
- SPI
- I²C
- I²S, mith master clock
- SD/eMMC, with 4 data lines
- SMBus
- LIN
- ISO-7816
- 1 UART for RS-232 with hardware flow control and RS-485/RS-422
- CAN

Other protocols are supported in software.
