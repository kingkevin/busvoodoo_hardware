# footprint for SOT363
# manufacturer: Diodes
# datasheet: https://www.diodes.com/assets/Package-Files/SOT363.pdf
Element["" "SOT363" "" "SOT363" 0 0 0 0 0 100 ""]
(
ElementLine[-0.675mm -1.1mm 0.675mm -1.1mm 0.2mm]
ElementLine[0.675mm -1.1mm 0.675mm 1.1mm 0.2mm]
ElementLine[0.675mm 1.1mm -0.675mm 1.1mm 0.2mm]
ElementLine[-0.675mm 1.1mm -0.675mm -1.1mm 0.2mm]
Pad[-1.04mm -0.65mm -0.86mm -0.65mm 0.42mm 0.4mm 0.52mm "" "1" "square"]
Pad[-1.04mm 0.0mm -0.86mm 0.0mm 0.42mm 0.4mm 0.52mm "" "2" "square"]
Pad[-1.04mm 0.65mm -0.86mm 0.65mm 0.42mm 0.4mm 0.52mm "" "3" "square"]
Pad[1.04mm -0.65mm 0.86mm -0.65mm 0.42mm 0.4mm 0.52mm "" "6" "square"]
Pad[1.04mm 0.0mm 0.86mm 0.0mm 0.42mm 0.4mm 0.52mm "" "5" "square"]
Pad[1.04mm 0.65mm 0.86mm 0.65mm 0.42mm 0.4mm 0.52mm "" "4" "square"]
)
