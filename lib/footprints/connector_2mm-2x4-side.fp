# footprint for an IDC 2x5 connector to be mounted on the side of the board
Element["" "IDC 2x10 side" "" "IDC 2x10 side" 0 0 0 0 0 100 ""]
(
ElementLine[0.0mm -3.95mm 1.5mm -3.95mm 0.2mm]
ElementLine[1.5mm -3.95mm 1.5mm 3.95mm 0.2mm]
ElementLine[1.5mm 3.95mm 0.0mm 3.95mm 0.2mm]
ElementLine[0.0mm 3.95mm 0.0mm -3.95mm 0.2mm]
Pad[-2.375mm -3.0mm -0.625mm -3.0mm 1.25mm 0.4mm 1.45mm "" "1" "square"]
Pad[-2.375mm -3.0mm -0.625mm -3.0mm 1.25mm 0.4mm 1.45mm "" "2" "square,onsolder"]
Pad[-2.375mm -1.0mm -0.625mm -1.0mm 1.25mm 0.4mm 1.45mm "" "3" "square"]
Pad[-2.375mm -1.0mm -0.625mm -1.0mm 1.25mm 0.4mm 1.45mm "" "4" "square,onsolder"]
Pad[-2.375mm 1.0mm -0.625mm 1.0mm 1.25mm 0.4mm 1.45mm "" "5" "square"]
Pad[-2.375mm 1.0mm -0.625mm 1.0mm 1.25mm 0.4mm 1.45mm "" "6" "square,onsolder"]
Pad[-2.375mm 3.0mm -0.625mm 3.0mm 1.25mm 0.4mm 1.45mm "" "7" "square"]
Pad[-2.375mm 3.0mm -0.625mm 3.0mm 1.25mm 0.4mm 1.45mm "" "8" "square,onsolder"]
)
