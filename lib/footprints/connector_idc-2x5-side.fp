# footprint for an IDC 2x5 connector to be mounted on the side of the board
Element["" "IDC 2x10 side" "" "IDC 2x10 side" 0 0 0 0 0 100 ""]
(
Pad[-3.25mm -5.08mm -0.75mm -5.08mm 1.5mm 0.4mm 1.7mm "" "1" "square"]
Pad[-3.25mm -5.08mm -0.75mm -5.08mm 1.5mm 0.4mm 1.7mm "" "2" "square,onsolder"]
Pad[-3.25mm -2.54mm -0.75mm -2.54mm 1.5mm 0.4mm 1.7mm "" "3" "square"]
Pad[-3.25mm -2.54mm -0.75mm -2.54mm 1.5mm 0.4mm 1.7mm "" "4" "square,onsolder"]
Pad[-3.25mm 0.0mm -0.75mm 0.0mm 1.5mm 0.4mm 1.7mm "" "5" "square"]
Pad[-3.25mm 0.0mm -0.75mm 0.0mm 1.5mm 0.4mm 1.7mm "" "6" "square,onsolder"]
Pad[-3.25mm 2.54mm -0.75mm 2.54mm 1.5mm 0.4mm 1.7mm "" "7" "square"]
Pad[-3.25mm 2.54mm -0.75mm 2.54mm 1.5mm 0.4mm 1.7mm "" "8" "square,onsolder"]
Pad[-3.25mm 5.08mm -0.75mm 5.08mm 1.5mm 0.4mm 1.7mm "" "9" "square"]
Pad[-3.25mm 5.08mm -0.75mm 5.08mm 1.5mm 0.4mm 1.7mm "" "10" "square,onsolder"]
)
