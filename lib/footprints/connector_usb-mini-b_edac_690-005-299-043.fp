# footprint for a USB mini-B socket
# manufacturer: EDAC
# part number: 690-005-299-043
# datasheet: http://files.edac.net/690-005-299-043.pdf
Element["" "USB mini-B" "" "EDAC 690-005-299-043" 0 0 0 0 0 100 ""]
(
Pad[2.033mm -1.6mm 3.567mm -1.6mm 0.5mm 0.4mm 0.7mm "" "1" "square"]
Pad[2.033mm -0.8mm 3.567mm -0.8mm 0.5mm 0.4mm 0.7mm "" "2" "square"]
Pad[2.033mm -0.0mm 3.567mm -0.0mm 0.5mm 0.4mm 0.7mm "" "3" "square"]
Pad[2.033mm 0.8mm 3.567mm 0.8mm 0.5mm 0.4mm 0.7mm "" "4" "square"]
Pad[2.033mm 1.6mm 3.567mm 1.6mm 0.5mm 0.4mm 0.7mm "" "5" "square"]
Pad[-3.2mm 4.45mm -2.7mm 4.45mm 2.0mm 0.4mm 2.2mm "" "6" "square"]
Pad[-3.2mm -4.45mm -2.7mm -4.45mm 2.0mm 0.4mm 2.2mm "" "6" "square"]
Pad[2.3mm 4.45mm 2.8mm 4.45mm 2.0mm 0.4mm 2.2mm "" "6" "square"]
Pad[2.3mm -4.45mm 2.8mm -4.45mm 2.0mm 0.4mm 2.2mm "" "6" "square"]
Pin[0.0mm 2.2mm 0.0mm 2.2mm 0.9mm 0.9mm "" "" "hole"]
Pin[0.0mm -2.2mm 0.0mm -2.2mm 0.9mm 0.9mm "" "" "hole"]
)
