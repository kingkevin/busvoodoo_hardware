Element["" "ABM3" "" "SMT crystal Abracon ABM3" 0 0 0 0 0 100 ""]
(
ElementLine[-2.5mm -1.6mm 2.5mm -1.6mm 0.2mm]
ElementLine[2.5mm -1.6mm 2.5mm 1.6mm 0.2mm]
ElementLine[2.5mm 1.6mm -2.5mm 1.6mm 0.2mm]
ElementLine[-2.5mm 1.6mm -2.5mm -1.6mm 0.2mm]
Pad[-2.05mm -0.25mm -2.05mm 0.25mm 1.9mm 0.4mm 2.1mm "" "1" "square"]
Pad[2.05mm -0.25mm 2.05mm 0.25mm 1.9mm 0.4mm 2.1mm "" "2" "square"]
)
